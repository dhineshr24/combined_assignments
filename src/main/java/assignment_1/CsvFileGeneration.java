package assignment_1;

import au.com.anthonybruno.Gen;
import com.github.javafaker.Faker;

import java.util.Locale;

class CsvFileGeneration{
    public void GenerateCSVs() {
        for(int i=1;i<=100;i++){
            Faker faker = new Faker(new Locale("en-IND"));
            Gen.start()
                    .addField("Name ", () -> faker.name().fullName())
                    .addField("Age ", () -> faker.number().numberBetween(18, 100))
                    .addField("Company ", () -> faker.company().name())
                    .addField("Building_Code", () -> faker.address().buildingNumber())
                    .addField("Phone_Number ", () -> faker.phoneNumber().cellPhone())
                    .addField("Address ", () -> faker.address().fullAddress())
                    .generate(11)
                    .asCsv()
                    .toFile("/Users/taurus/Desktop/Assignment/src/main/java/assignment_1/CsvFiles/PFile_"+i+".csv");
            }
        }
}
