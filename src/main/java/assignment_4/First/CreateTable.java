package assignment_4.First;
import java.io.IOException;

//made constant variables final
public class CreateTable {
    public static void main(String[] args) throws IOException {
        final String tableName = "EmployeeProto";
        final String columnFamilyName = "employeeFamily";
        Utils hbaseObj = new Utils(tableName, columnFamilyName);
        try{
            hbaseObj.createTable();
            hbaseObj.close();
        } catch (IOException e) {
            e.printStackTrace();
        }

        //test to check if data is inserted correctly by mapper task
//        Connection hbaseCon = ConnectionFactory.createConnection(new HBaseConfiguration().create());
//        Table table = hbaseCon.getTable(TableName.valueOf(Bytes.toBytes("EmployeeProto")));
//        for(int buildingNumber = 1; buildingNumber <= 1; buildingNumber++){
//            Result result = table.get(new Get(Bytes.toBytes("EMP1425")));
//            byte [] value = result.getValue(Bytes.toBytes("employeeFamily"),Bytes.toBytes("ProtoObject"));
//            EmployeeProto.EmployeeDatabase dbo = EmployeeProto.EmployeeDatabase.parseFrom(value);
//            System.out.println(dbo);
//            //buildingInfo.put(dbo.getBuilding(0).getBuildingCode(),dbo.getBuilding(0).getCafteriaCode());
//
//        }




    }

}
