package assignment_4.First;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.hbase.HBaseConfiguration;
import org.apache.hadoop.hbase.TableName;
import org.apache.hadoop.hbase.client.*;
import org.apache.hadoop.hbase.util.Bytes;

import java.io.IOException;

//Added specific exception handling in catch instead of generic exceptions
public class Utils {
    private static byte[] tableName = null;
    private static byte[] COLUMN_FAMILY = null;
    private static final Configuration config = HBaseConfiguration.create();
    private static Connection conn;
    private static TableName TABLE_NAME;
    private static Admin admin;
    private Table table = null;
    public Utils(String newTableName, String columnFamily){
        try{
            tableName = Bytes.toBytes(newTableName);
            COLUMN_FAMILY = Bytes.toBytes(columnFamily);
            conn = ConnectionFactory.createConnection(config);
            TABLE_NAME = TableName.valueOf(tableName);
            admin  = conn.getAdmin();
            table = conn.getTable(TABLE_NAME);
            System.out.println(table);
        }
        catch (IOException e) {
            e.printStackTrace();
        }
    }
    //Checks if table already exists, otherwise creates it
    public void createTable() throws IOException {
        try {
            if (!admin.tableExists(TABLE_NAME)) {
                TableDescriptor desc = TableDescriptorBuilder.newBuilder(TABLE_NAME)
                        .setColumnFamily(ColumnFamilyDescriptorBuilder.of(COLUMN_FAMILY))
                        .build();
                admin.createTable(desc);
                System.out.println("Table created");
            }
        }
        catch(IOException ex){
            ex.printStackTrace();
        }
    }
    public void close() throws IOException {
        table.close();
        admin.close();
        conn.close();
    }
}
