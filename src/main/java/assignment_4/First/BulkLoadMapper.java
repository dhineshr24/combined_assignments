package assignment_4.First;

import protoPackage.AttendanceProto;
import protoPackage.BuildingProto;
import protoPackage.EmployeeProto;
import org.apache.hadoop.hbase.client.Put;
import org.apache.hadoop.hbase.io.ImmutableBytesWritable;
import org.apache.hadoop.hbase.util.Bytes;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Mapper;

import java.io.IOException;

//made constant variables final
public class BulkLoadMapper extends Mapper<LongWritable,Text,ImmutableBytesWritable,Put> {
    private static final String employeeTableName = "EmployeeProtoTemp";
    private static final String buildingTableName = "BuildingProtoTemp";
    private static final String employeeColumnFamily = "employeeFamily";
    private static final String buildingColumnFamily = "buildingFamily";
    private static final String attendanceTableName = "AttendanceProto";
    private static final String attendanceColumnFamily = "attendanceFamily";

    private static final String columnName = "ProtoObject";
    @Override
    protected void map(LongWritable key, Text value, Context context) throws IOException, InterruptedException {
        final String splitWith = ",";
        ProtoBufUtils util = new ProtoBufUtils();
        String record = value.toString();
        String[] split = record.split(splitWith);
        if (context.getJobName().equals(attendanceTableName+"BulkLoad"))
        {
            if(!split[0].equals("Employee_id")) {
                //Generating HFiles for Attendance Proto Table
                AttendanceProto.AttendanceDatabase attendanceDb = util.getAttendanceDBObj(split);
                //Printing objects before inserting to the DB
                int attendanceCount = attendanceDb.getAttendanceCount();
                System.out.println("LOG_INFO: Printing objects before inserting to the DB.");
                for (int i = 0; i < attendanceCount; i++) {
                    System.out.println(attendanceDb.getAttendance(i).getAllFields());
                }
                Put put = new Put(Bytes.toBytes(split[0]));
                put.addColumn(Bytes.toBytes(attendanceColumnFamily),
                        Bytes.toBytes(columnName), attendanceDb.toByteArray());
                context.write(new ImmutableBytesWritable(Bytes.toBytes(attendanceTableName)), put);
            }
        }
        else if(context.getJobName().equals(employeeTableName+"BulkLoad")){
            if(split[4].equals("Salary") == false) {
                EmployeeProto.EmployeeDatabase obj = util.getEmpDBObj(split);
                Put put = new Put(Bytes.toBytes(split[0]));
                put.addColumn(Bytes.toBytes(employeeColumnFamily),
                        Bytes.toBytes(columnName), obj.toByteArray());
                context.write(new ImmutableBytesWritable(Bytes.toBytes(employeeTableName)),put);
            }
        }
        else if(split.length == 4){
            if(split[0].equals("building_code") == false) {
                BuildingProto.BuildingDatabase obj = util.getBuildingDBObj(split);
                Put put = new Put(Bytes.toBytes(split[0]));
                put.addColumn(Bytes.toBytes(buildingColumnFamily),
                        Bytes.toBytes(columnName), obj.toByteArray());
                context.write(new ImmutableBytesWritable(Bytes.toBytes(buildingTableName)),put);
            }
        }

    }
}